package main

import (
	"gitlab.com/DhritiShikhar/matdaan/Models"
	"gitlab.com/DhritiShikhar/matdaan/Routes"
)

func main() {
	// Connect to database
	Models.ConnectDatabase()

	// Routes
	r := Routes.Setup()

	// Run the server
	r.Run()
}
