package Routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/DhritiShikhar/matdaan/Controllers"
)

func Setup() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("/v1")

	{
		v1.GET("matdaan", Controllers.GetMatdaan)
	}
	return r
}
