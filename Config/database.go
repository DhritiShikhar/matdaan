package Config

import (
	"fmt"

	"gorm.io/gorm"
)

var DB *gorm.DB

type DBConfig struct {
	DBName string
	Host   string
	Port   int
	User   string
}

func BuildDBConfig() *DBConfig {
	dbConfig := DBConfig{
		Host:   "0.0.0.0",
		Port:   5432,
		User:   "postgres",
		DBName: "matdaan",
	}
	return &dbConfig
}

func DbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
		"host=%s port=%v user=%s dbname=%s sslmode=disable",
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.User,
		dbConfig.DBName,
	)
}
