package Models

type Matdaan struct {
	ID         int    `json:"id,omitempty"`
	Contestant string `json:"contestant,omitempty"`
	Vote       int    `json:"vote,omitempty"`
}

func (matdaan *Matdaan) TableName() string {
	return "matdaan"
}
