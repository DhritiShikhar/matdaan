package Models

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/DhritiShikhar/matdaan/Config"
)

func GetMatdaan(matdaan *Matdaan) (err error) {
	if err := Config.DB.Find(matdaan).Error; err != nil {
		return err
	}
	return nil
}
