package Models

import (
	"fmt"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/DhritiShikhar/matdaan/Config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDatabase() {
	database, err := gorm.Open(postgres.Open(Config.DbURL(Config.BuildDBConfig())), &gorm.Config{})
	if err != nil {
		fmt.Println("status: ", err)
	}

	db, err := database.DB()
	if err != nil {
		fmt.Println("status: ", err)
	}

	defer db.Close()

	database.AutoMigrate(&Matdaan{})

	Config.DB = database
}
