module gitlab.com/DhritiShikhar/matdaan

go 1.15

require (
	github.com/gin-gonic/gin v1.7.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jinzhu/gorm v1.9.16
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.20.12
)
