package Controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/DhritiShikhar/matdaan/Models"
)

// GetMatdaan returns matdaan information
func GetMatdaan(c *gin.Context) {
	var matdaan Models.Matdaan

	Models.GetMatdaan(&matdaan)

	fmt.Println(">>>>>>>>>>>>>>>>>>", &matdaan)

	c.JSON(http.StatusOK, gin.H{"data": matdaan})
}
